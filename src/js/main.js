/**
 * Main
 * Custom Scripts
 */

$(function() {
  var slider = tns(
    {
    container: '.my-slider',
    controls:false,
    items: 1,
    navPosition: "bottom",
    navContainer: ".slider-nav"
  });
  var slider2 = tns({
    container: '.blog-slider',
    controls: false,
    items: 2,
    slideBy:1,
    autoplay: true,
    axis: 'vertical',
    nav: false,
    autoplayButtonOutput: false,
  });
})

$(window).smartresize(function() {
  // Debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
});


