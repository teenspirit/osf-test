$( document ).ready(function() {
    var apiurl;
    apiurl = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=f0b84fba1c00631410b85b90720f52ba&text=nature&per_page=6&format=json&nojsoncallback=1";
    $.getJSON(apiurl, function (json) {
        $.each(json.photos.photo, function (i, myresult) {
            var url = 'https://farm' + myresult.farm + ".staticflickr.com/" + myresult.server + '/' + myresult.id + '_' + myresult.secret + '_b.jpg';


            $('<img>').attr("src", url).height("60").width("76").attr("alt",myresult.title).appendTo(".widget--flickr .widget__content");

            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            //var img = document.getElementsByClassName('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal.style.display = "none";
            }

            $("img").click(function () {
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            });
        });
    });
    
});
$(document).click(function(event) {
    //if you click on anything except the modal itself or the "open modal" link, close the modal
    if (!$(event.target).closest(" .widget--flickr img").length) {
        $(".modal").css('display', "none");
     
    }
  });