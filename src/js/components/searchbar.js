'use strict';

var searchWrapper = document.querySelector('.header__inner__search'),
     searchInput = document.querySelector('.search-input'),
     searchDrop = document.querySelector('.search__dropdown');


document.addEventListener('click',function (e) {
  if (~e.target.className.indexOf('search') && !searchWrapper.classList.contains('focused'))  {
    searchWrapper.classList.add('focused');
    searchInput.focus();
    searchDrop.classList.add('visible');
  } else {
    searchWrapper.classList.remove('focused');
    searchDrop.classList.remove('visible');
  }
})



$('.submit').click(function(){
  var target = $(this);
  var lastInputContainerLabel = target.parent().find('.span-container');
  target.addClass('submitted');
  lastInputContainerLabel.addClass('fadeOut');
})


$(window).resize(function() {
  if ($(window).width() < 760) {
      
  var searchWrapper1 = document.querySelector('.item--search'),
  searchInput1 = document.querySelector('.item--search .search-input-i'),
  searchDrop1 = document.querySelector('.item--search .search__dropdown-i');


 $('.item--search').on('click',function (e) {
  if (~e.target.className.indexOf('search') && !searchWrapper.classList.contains('focused'))  {
      searchWrapper1.classList.add('focused');
      searchInput1.focus();
      searchDrop1.classList.add('visible');
      } else {
      searchWrapper1.classList.remove('focused');
      searchDrop1.classList.remove('visible');
      }
  });
    }
 
});

$(document).ready(function(){
  $.ajaxSetup({ cache: false });
  $('#autocomplete').keyup(function(){
   $('.search__dropdown').html('');
   var searchField = $('#autocomplete').val();
   var expression = new RegExp(searchField, "i");
   $.getJSON('resources/categories.json', function(data) {
    $.each(data, function(key, value){
     if (value.name.search(expression) != -1 )
     {
      $('.search__dropdown').toggleClass('visible').append('<a href="'+value.url+'">'+value.name+'</a>');
     }
    });   
   });
  });
  
  $('.search__dropdown').on('click', function() {
   var click_text = $(this).text().split('|');
   $('#autocomplete').val($.trim(click_text[0]));
   $(".search__dropdown").html('');
  });
 });